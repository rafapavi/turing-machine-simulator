from Transition import Transition
class State:

	def __init__(self, state_id, final = False):
		"""
		params:			(String) state_id, the state of 
						(Boolean) final, whether the state is Final or not
		"""
		self.__state_id = state_id
		self.__transitions = {}
		self.__final_state = final

	def get_stateID(self):
		return self.__state_id

	def is_final_state(self):
		return self.__final_state

	def set_final(self):
		self.__final_state = True
		
	def add_transition(self, transition):
		self.__transitions[transition.get_read()] = transition

	"""
	def add_transition(self, read_bit, write_bit, next_state, direction):
		new_tran = Transition(read_bit, write_bit, next_state)
		self.__transitions[read_bit] = new_tran
	"""

	def get_transition(self, read_bit):
		if (read_bit in self.__transitions):
			return self.__transitions[read_bit]
		else:
			return None
		
