import xml.etree.ElementTree as ET
from State import State
from Transition import Transition
from turingmachine import *
from kivy.uix.textinput import TextInput
from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.floatlayout import FloatLayout
from kivy.graphics import Color, Ellipse, Rectangle, Line
from kivy.properties import ListProperty, BooleanProperty, NumericProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.uix.button import Button
from kivy.core.window import Window

# Widget holding the canvas for graph building
class TuringCanvas(Widget):

    def starter(self):
    	Content = BoxLayout()
    	LoadButton = Button(text = 'Load', font_size = 14)
    	CreateNewButton = Button(text = 'Create New', font_size = 14)
    	self.NewLabel = TextInput()
    	Content.orientation = 'vertical'
    	Content.add_widget(Label(text = 'Enter XML File Name: '))
    	Content.add_widget(self.NewLabel)
    	Content.add_widget(LoadButton)
    	Content.add_widget(CreateNewButton)
    	self.startup = Popup(title = 'Input of XML file ', content = Content, size_hint = (None, None), size = (200, 200), auto_dismiss = False)
    	self.startup.open()
    	LoadButton.bind(on_press = self.parseXML)
    	CreateNewButton.bind(on_press = self.createNewXML)
    	

    def addStateCircle(self, object):
		state_ID = self.NewStateLabel.text
		newState = State(state_ID)
		if (state_ID not in self.TM.get_state_keys()):
			self.TM.add_state(newState)	

		newStateShape = StateObjectGUI(state_ID, self.state_x, self.state_y)
		#Repositioning the Items
		if (len(self.TM.get_state_keys()) % 4 == 0):
			print ("fsdfs")
			self.state_y += 80
			self.state_x = 50
		else:
			self.state_x += 80

		self.add_widget(newStateShape)
		self.statePop.dismiss()

    def addStateObj(self, object):
		Content = BoxLayout()
		CreateNewButton = Button(text = 'Create New', font_size = 14)
		self.NewStateLabel = TextInput()
		Content.orientation = 'vertical'
		Content.add_widget(Label(text = 'Enter State Name/ID: '))
		Content.add_widget(self.NewStateLabel)
		Content.add_widget(CreateNewButton)
		self.statePop = Popup(title = 'New State', content = Content, size_hint = (None, None), size = (200, 200), auto_dismiss = False)
		self.statePop.open()
		CreateNewButton.bind(on_press = self.addStateCircle)

    def initialize(self):
		newStateButton = Button(text = 'Create State', size = (100, 60))
		newStateButton.bind(on_release = self.addStateObj)
		self.state_x = 50
		self.state_y = 300
		self.add_widget(newStateButton)

    def createNewXML(self, object):
		filename = self.NewLabel.text + ".xml"
		self.startup.dismiss()
		root = ET.Element('turingmachine')
		tree = ET.ElementTree(root)
		tree.write(filename)
		self.TM = TuringMachine()
		self.initialize()
		
    def parseXML(self, object):
		filename = self.NewLabel.text
		try:
			alphabets = []

			tree = ET.parse(filename)
			root = tree.getroot()

			#Alphabets
			for i in root.findtext("alphabet"):
				alphabets.append(i)

			#Initial Tape
			tape = root.findtext("initialtape")

			#Blank Chars
			blank = root.find("blank").get("char")

			#initial state
			init_state = root.find("initialstate").get("name")
			
			#Final State
			final_states = []
			final_states_string = root.find("finalstates")
			for i in final_states_string.findall("finalstate"):
				print(i)
				final_states.append(i.attrib["name"])

			
			#Initializing the Turing Machine Simulator
			the_simulator = TuringMachine(alphabets, init_state, final_states, blank, tape)

			#Getting the state names
			subroot = root.find("states")
			for state in subroot.findall("state"):
				if (state.get("name") in the_simulator.get_final_states()	):
					the_state = State(state.get("name"), True)
				else:
	 				the_state = State(state.get("name"))

				for info in state:
					the_transition = Transition(info.get("seensym"), info.get("writesym"), info.get("move"), info.get("newstate"))
					the_state.add_transition(the_transition)

				the_simulator.add_state(the_state)

			return the_simulator

		except IOError:
			print "File does not exist"

		except ET.ParseError:
			print "File must be an xml"
			return None

class StateObjectGUI(Widget):
	remove_on_drag = BooleanProperty(True)
	drag_opacity = NumericProperty(1.0)
	
	def __init__(self, state_id, x, y):
		self.register_event_type("on_drag_start")
		self.register_event_type("on_being_dragged")
		self.register_event_type("on_drag_finish")

		self.state = state_id
		self.x = x
		self.y = y
		self._dragged = False
		self._draggable = True
		self.radius = 40
		return super(StateObjectGUI, self).__init__()

	def set_dragable(self, value):
		self._draggable = value
	
	def set_remove_on_drag(self, value):
		self.remove_on_drag = value
		
	def on_touch_down(self, touch):
		super(StateObjectGUI, self).on_touch_down(touch)
		print (self)
		if (self.collide_point(touch.x, touch.y)):
			self.dispatch("on_drag_start")
			self.xdistance = touch.x - self.pos[0]
			self.ydistance = touch.y - self.pos[1]
			print ("fadsfsd")

	def on_touch_move(self, touch):
		super(StateObjectGUI, self).on_touch_down(touch)

		if (self._draggable and self._dragged):
			x = touch.x - self.xdistance
			y = touch.y - self.ydistance

			self.pos = (x, y)		
		
	def on_being_dragged(self):
		print "being dragged"

	def on_touch_up(self, touch):
		if (self._draggable and self._dragged):
			self.short_touch = True
			self.dispatch("on_drag_finish")
			self.short_touch = False

	def on_drag_start(self):
		self.opacity = self.drag_opacity
		#self.set_bound_axis_positions()
		self._old_drag_pos = self.pos
		self._old_parent = self.parent
		self._old_index = self.parent.children.index(self)
		self._dragged = True

	def on_drag_finish(self):
		if (self._dragged and self._draggable):
			self.remove_on_drag=True
			self.opacity = 1.0
			dropped_ok = False
			self._dragged = False
	
	def reparent(self, widget):
		parent = widget.parent
		orig_size = widget.size
		if parent:
			parent.remove_widget(widget)
			parent.get_root_window().add_widget(widget)
			widget.size_hint = (None, None)
			widget.size = orig_size

class TuringMachineApp(App):

	def build(self):
		parent = TuringCanvas()
		parent.starter()

		return parent
		

if (__name__ == "__main__"):
	TuringMachineApp().run()