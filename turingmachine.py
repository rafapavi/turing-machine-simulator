import xml.etree.ElementTree as ET
from State import State
from Transition import Transition
from Tape import Tape

class TuringMachine:
	def __init__(self, alphabets = [], initial_state = "", final_states = [], blank = "", initial_tape = ""):
		"""
		params:			(List<String>) alphabets, list that represents the applicable alphabets
						(String) initial_state, represents the starting state of the TuringMachine
						(List<String>) final_states, contains all the state names/IDs of the final states
						(String) blank, represents the blank character of the Turing Machine
						(String) initial_tape, the initial tape of the Turing Machine
		"""
		self.__alphabets = alphabets
		self.__states = {}
		self.__initial_state = initial_state
		self.__current_state = self.__initial_state
		self.__final_states = final_states
		self.__tape = Tape(initial_tape, blank)

	def get_final_states(self):
		return self.__final_states

	def add_state(self, state):
		"""
		Adds to the State Dictionary
		"""
		self.__states[state.get_stateID()] = state

	def get_current_state(self):
		return self.__current_state

	def get_state_keys(self):
		return list(self.__states)

	def get_tape(self):
		return self.__tape.gettape()

	def run_step(self):
		active_cell = self.__tape.get_current_head()
		state = self.__states[self.__current_state]
		transition = state.get_transition(active_cell)
		if (transition is None):
			return False
		self.__current_state = transition.get_next_state()
		if (transition.getwrite() in self.__alphabets):
			return False
		if (transition.get_direction() == "R"):
			self.__tape.writeright(transition.get_write())
		else:
			self.__tape.writeleft(transition.get_write())

		return True


	def run_turing(self, filename):
		output_file = filename.rstrip("xml") + "trace"
		the_file = open(output_file, "w")

		steps = 0
		print "Initiate State: " + self.__initial_state
		print "Initiate Step: " + self.__tape.get_tape() + "\n"
		the_file.write("Initiate State: " + self.__current_state + "\n")
		the_file.write("Initiate Step: " + self.__tape.get_tape() + "\n" + "\n")

		while self.run_step():
			steps += 1
			print "Steps: " + str(steps)
			print "Current State: " + self.__current_state
			print "Current Tape: " + self.__tape.get_tape() + "\n"
			the_file.write("Steps: " + str(steps) + "\n")
			the_file.write("Current State: " + self.__current_state + "\n")
			the_file.write("Current Tape: " + self.__tape.get_tape() + "\n" + "\n")

		if (self.__current_state in self.__final_states):
			print "YES, ended with halt"
			the_file.write("YES, ended with halt" + "\n")
		else:
			print "NO, ended without halt"
			the_file.write("NO, ended without halt" + "\n")

		#editXML(self.__tape.get_tape_string(), self.__current_state, tree, filename)

def parseXMLFile(filename):
	try:
		alphabets = []

		tree = ET.parse(filename)
		root = tree.getroot()

		#Alphabets
		for i in root.findtext("alphabet"):
			alphabets.append(i)

		#Initial Tape
		tape = root.findtext("initialtape")

		#Blank Chars
		blank = root.find("blank").get("char")

		#initial state
		init_state = root.find("initialstate").get("name")
		
		#Final State
		final_states = []
		final_states_string = root.find("finalstates")
		for i in final_states_string.findall("finalstate"):
			print(i)
			final_states.append(i.attrib["name"])

		
		#Initializing the Turing Machine Simulator
		the_simulator = TuringMachine(alphabets, init_state, final_states, blank, tape)

		#Getting the state names
		subroot = root.find("states")
		for state in subroot.findall("state"):
			if (state.get("name") in the_simulator.get_final_states()	):
				the_state = State(state.get("name"), True)
			else:
 				the_state = State(state.get("name"))

			for info in state:
				the_transition = Transition(info.get("seensym"), info.get("writesym"), info.get("move"), info.get("newstate"))
				the_state.add_transition(the_transition)

			the_simulator.add_state(the_state)

		return the_simulator

	except IOError:
		print "File does not exist"

	except ET.ParseError:
		print "File must be an xml"
		return None

def editXML(tape, last_state, tree, filename):
    """
    Create an example ET file
    """

    root = tree.getroot()
    
    #Adding last tape
    final_tape = ET.Element("finaltape")
    final_tape.text = "".join(tape)
    root.append(final_tape)


    #Adding end state
    final_state = ET.Element("finalstate")
    final_state.text = last_state
    root.append(final_state)

    indent(root)

    #Changing file name 
    filename = filename.rstrip(".xml") + "_output.xml"

    tree = ET.ElementTree(root)
    with open(filename, "w") as fh:
        tree.write(fh)

    #tree.write(filename)

def indent(elem, level=0):
	i = "\n" + level*"  "
	if len(elem):
		if not elem.text or not elem.text.strip():
			elem.text = i + "  "
		if not elem.tail or not elem.tail.strip():
			elem.tail = i
		for elem in elem:
			indent(elem, level + 1)
		if not elem.tail or not elem.tail.strip():
			elem.tail = i
	else:
		if level and (not elem.tail or not elem.tail.strip()):
			elem.tail = i

def driver():
	tms = parseXMLFile(filename)
	if (tms is not None):
		tms.run_turing(filename)


