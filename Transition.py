class Transition:
	def __init__(self, read, write, move, next_state):
		"""
		params:			(String) read, the symbol read on the tape.
						(String) write, the symbol to be written on the tape.
						(String) move, movement whether L or R which is left or right for the header to move.
						(String) next_state, the next state the machine is at.
		"""
		self.__read = read
		self.__write = write
		self.__direction = move 
		self.__next_state = next_state
		#self.__current_state = current_state

	def get_read(self):
		return self.__read

	def get_write(self):
		return self.__write

	def get_direction(self):
		return self.__direction

	def get_next_state(self):
		return self.__next_state

	def set_read(self, new_read_bit): #changes the read symbol on xml.
		set_read = new_read_bit

	def set_write(self, new_write_bit): #changes the write symbol on xml.
		self.__write = new_write_bit

	def set_direction(self, new_dir):	# changes the move on xml.
		self.__direction = new_dir

	def set_next_state(self, new_state): 	#changes the next state on the xml.
		self.__next_state = new_state


