class Tape:
	def __init__(self, initial_tape, blank_char):
		"""
		params:			(String) initial_tape, the initial tape of the Turing Machine
						(String) blank_char, represents the blank character of the Turing Machine
		"""
		self.__current_tape = list(initial_tape)
		self.__pos = 0
		self.__blank_char = blank_char 
		

	def get_tape(self):		
		current_tape = ""
		for i in range(len(self.__current_tape)):	
			if(i == self.__pos):		#indicates the current symbol view by using * *
				current_tape += "*" + self.__current_tape[i] + "*"
			else:
				current_tape += self.__current_tape[i]

		return current_tape

	def get_tape_string():	#getting the tape.
		return "".join(self.__current_tape)

	def get_current_head(self): #getting the current symbol the header is at.
		return self.__current_tape[self.__pos]

	def writeleft(self, newchar):
		self.__current_tape[self.__pos] = newchar
		if (self.__pos == 0):
			self.__current_tape.insert(0, self.__blank_char)
		else:
			self.__pos -= 1

	def writeright(self, newchar):
		self.__current_tape[self.__pos] = newchar
		self.__pos += 1
		if self.__pos == len(self.__current_tape):
			self.__current_tape.append(self.__blank_char)
